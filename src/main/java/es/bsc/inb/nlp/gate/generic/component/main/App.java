package es.bsc.inb.nlp.gate.generic.component.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.ArrayUtils;
import org.apache.maven.shared.utils.io.FileUtils;

import gate.Gate;
import gate.creole.Plugin;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

/**
 * Generic Library for execute GATE DefaultGazetteer and FlexibleGazetter and JAPE rules processing in batch mode.
 *
 */ 
public class App {
	
	
    public static void main( String[] args ){

    	Options options = new Options();
    	
        Option input = new Option("i", "input", true, "input directory path");
        input.setRequired(true);
        options.addOption(input);
        
        Option output = new Option("o", "output", true, "output directory path");
        output.setRequired(true);
        options.addOption(output);
        
        Option listDefinitions = new Option("l", "listsURL", true, "Dictionary List definitions. "
        		+ "A lists.def Gate-formatted file separated by tab can be provided or a zip file that contains the dictionary/gazetteer files including the lists.def. "
        		+ " More than one .def files can be provided separated by , (i.e: lists.def,lists2.def) this configuration will generate two steps of gazetter annotation. ");
        listDefinitions.setRequired(false);
        options.addOption(listDefinitions);
        
        Option gazetterType = new Option("gt", "gazetter_type", true, "Gazetter type:  default, flexible.  If no value is provided the DefautlGazetter is used");
        gazetterType.setRequired(false);
        options.addOption(gazetterType);
        
        Option inputFeatureNames = new Option("inputFeatureNames", "inputFeatureNames", true, "See flexible gazetter required fields.  These feature values are used to replace the corresponding original text. "
        		+ " Default vales are Token.root,Token.word. Format if there is more than one feature: Token.xxx,Token.yyy");
        inputFeatureNames.setRequired(false);
        options.addOption(inputFeatureNames);
        
        Option japeMain = new Option("j", "jape_main", true, "Jape Main file for processing rules");
        japeMain.setRequired(false);
        options.addOption(japeMain);
        
        Option set = new Option("a", "outputASName", true, "Output Annotation Set. Annotation set where the annotation will be included for the gazetter lookup and for the Jape Rules");
        set.setRequired(true);
        options.addOption(set);
        
        Option iset = new Option("ia", "inputASName", true, "Input Annotation Set. If you want to provided different input annotation set this parameter.  By default the -a output annotation set is used as input.");
        iset.setRequired(false);
        options.addOption(iset);
        
        Option gazetteerFeatureSeparator = new Option("gazetteerFeatureSeparator", "gazetteerFeatureSeparator", true, "The character used to add arbitrary features to gazetteer entries. Default tab");
        gazetteerFeatureSeparator.setRequired(false);
        options.addOption(gazetteerFeatureSeparator);
        
        Option caseSensitive = new Option("caseSensitive", "caseSensitive", true, "Should the gazetteer be case sensitive during matching. Default false");
        caseSensitive.setRequired(false);
        options.addOption(caseSensitive);
        
        Option longestMatchOnly = new Option("longestMatchOnly", "longestMatchOnly", true, "This parameter is only relevant when the list of lookups contains proper preﬁxes "
        		+ "of other entries (e.g when both ‘Body Weight’ and ‘Body Weight loss’ are in the lists). The default behaviour (when this parameter is set to true) is to only match the longest entry, ‘Body Weight loss’ in this example. "
        		+ "Setting this parameter to false will cause the gazetteer to match all possible preﬁxes.");
        longestMatchOnly.setRequired(false);
        options.addOption(longestMatchOnly);
        
        Option workdir = new Option("w", "workdir", true, "workDir directory path");
        workdir.setRequired(false);
        options.addOption(workdir);
        
        Option threads_option = new Option("t", "threads", true, "Threads to be used. The documents are splited for data parallelization");
        threads_option.setRequired(false);
        options.addOption(threads_option);
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;
    	try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        }
    	
    	Map<String,String> parameters = new HashMap<String,String>();
    	
    	
        String outputFilePath = cmd.getOptionValue("output");
        String workdirPath = cmd.getOptionValue("workdir");
        String listsDefinitionsPath = cmd.getOptionValue("listsURL");
        String japeMainPath = cmd.getOptionValue("jape_main");
        String threads_str = cmd.getOptionValue("threads");
        
        if (!java.nio.file.Files.isDirectory(Paths.get(cmd.getOptionValue("input")))) {
    		System.out.println(" Please set the inputDirectoryPath ");
			System.exit(1);
		}
        parameters.put("inputFilePath", cmd.getOptionValue("input"));
       
        if (cmd.getOptionValue("gazetter_type")==null) {
        	System.out.println("The DefaultGazetter will be used");
        	parameters.put("gazetter_type", "default");
		}else if(cmd.getOptionValue("gazetter_type").equals("default") || cmd.getOptionValue("gazetter_type").equals("flexible")){
			parameters.put("gazetter_type", cmd.getOptionValue("gazetter_type"));
			System.out.println("The Gazetter to be used: " + cmd.getOptionValue("gazetter_type"));
		}else {
			System.out.println("Wrong Gazetter Type configuration (default or flexible): " + cmd.getOptionValue("gazetter_type"));
		}

        parameters.put("inputFeatureNames", cmd.getOptionValue("inputFeatureNames"));
        
        if (cmd.getOptionValue("outputASName")==null) {
        	System.out.println("Please set the annotation set where the annotation will be included");
			System.exit(1);
    	}
        parameters.put("outputASName", cmd.getOptionValue("outputASName"));
        
        if (cmd.getOptionValue("inputASName")==null) {
        	System.out.println("The input annotation set not set, same as output is selected");
        	parameters.put("inputASName", cmd.getOptionValue("outputASName"));
		}else {
			parameters.put("inputASName", cmd.getOptionValue("inputASName"));
		}
        
        if (cmd.getOptionValue("gazetteerFeatureSeparator")==null) {
        	parameters.put("gazetteerFeatureSeparator", "\t");
    	}else {
    		parameters.put("gazetteerFeatureSeparator", cmd.getOptionValue("gazetteerFeatureSeparator"));
    	}
        
        if (cmd.getOptionValue("caseSensitive")==null) {
        	parameters.put("caseSensitive", "false");
    	}else {
    		parameters.put("caseSensitive", cmd.getOptionValue("caseSensitive"));
    	}
        
        if (cmd.getOptionValue("longestMatchOnly")==null) {
        	parameters.put("longestMatchOnly", "true");
    	}else {
    		parameters.put("longestMatchOnly", cmd.getOptionValue("longestMatchOnly"));
    	}
        
        if(workdirPath==null) {
    		workdirPath = "";
		}

        Boolean execution = false;
        
        if(listsDefinitionsPath==null) {
        	System.out.println("No dictionary was provided.");
        }else {
        	String[] list_defs = listsDefinitionsPath.split(",");
        	StringBuilder result = new StringBuilder();
        	for (String lf : list_defs) {
        		lf = workdirPath+lf;
        		System.out.println("Dictionary Path " + lf);
        		execution = true;
        		if (!java.nio.file.Files.isRegularFile(Paths.get(lf))) {
            		System.out.println("Please set the list of dictionaries to annotate in the correct format. You can provide the list.def file (or files , separated) or a zip file. Please if you provided a zip file remember that it must contain a list.def file inside");
        			System.exit(1);
            	}
        		if(lf.endsWith(".zip")) {
                	try {
        	       		File file = new File(lf);
        	       		String dictionaryFolderPath =  file.getName().substring(0, file.getName().indexOf(".zip"));
        	       		unZipIt(lf,  workdirPath  + dictionaryFolderPath );
        	       		lf = workdirPath  + dictionaryFolderPath + File.separator + "lists.def";
        	       		if (!java.nio.file.Files.isRegularFile(Paths.get(lf))) {
        	               	System.out.println("Please if you provided a zip file remember that it must contain a list.def file inside.");
        	               	System.out.println(lf);
        	       			System.exit(1);
        	           	}
        	       	}catch(Exception e) {
                       	System.out.println("Error unziping directory, please if you provided a zip file remember that it must contain a list.def file inside. ");
                       	System.exit(1);
                    }
                }else if(!lf.endsWith(".def")) {
                	System.out.println(" Please set the list of dictionaries to annotate.  No list.def file or .zip file provided.");
                	System.out.println(lf);
                	System.exit(1);
                }
        		result.append(lf);
        		result.append(",");
			}
        	listsDefinitionsPath = result.length() > 0 ? result.substring(0, result.length() - 1): "";
        	
//        	listsDefinitionsPath = workdirPath+listsDefinitionsPath;
//        	System.out.println("Dictionary Path " + listsDefinitionsPath);
//        	execution = true;
//        	if (!java.nio.file.Files.isRegularFile(Paths.get(listsDefinitionsPath))) {
//        		//System.out.println("Please set the list of dictionaries to annotate. You can provide the list.def file or a zip file. Please if you provided a zip file remember that it must contain a list.def file inside");
//    			//System.exit(1);
//        		//remember to validate with comma separated
//            }
//        	String[] list_defs_aux = listsDefinitionsPath.split(",");
//        	StringBuilder result_aux = new StringBuilder();
//        	for (String lf : list_defs_aux) {
//        		if(lf.endsWith(".zip")) {
//                	try {
//        	       		File file = new File(lf);
//        	       		String dictionaryFolderPath =  file.getName().substring(0, file.getName().indexOf(".zip"));
//        	       		unZipIt(lf,  workdirPath  + dictionaryFolderPath );
//        	       		lf = workdirPath  + dictionaryFolderPath + File.separator + "lists.def";
//        	       		if (!java.nio.file.Files.isRegularFile(Paths.get(lf))) {
//        	               	System.out.println("Please if you provided a zip file remember that it must contain a list.def file inside.");
//        	               	System.out.println(lf);
//        	       			System.exit(1);
//        	           	}
//        	       	}catch(Exception e) {
//                       	System.out.println("Error unziping directory, please if you provided a zip file remember that it must contain a list.def file inside. ");
//                       	System.exit(1);
//                    }
//                }else if(!lf.endsWith(".def")) {
//                	System.out.println(" Please set the list of dictionaries to annotate.  No list.def file or .zip file provided.");
//                	System.out.println(lf);
//                	System.exit(1);
//                }
//        		result.append(lf);
//        		result.append(",");
//        	}
        }
        
        parameters.put("listsURL", listsDefinitionsPath);
        
        if(japeMainPath==null) {
        	System.out.println("No Jape Main Rules were provided.");
        }else {
        	japeMainPath = workdirPath+japeMainPath;
        	execution = true;
        	if (!java.nio.file.Files.isRegularFile(Paths.get(japeMainPath))) {
        		System.out.println("Please set a correct path to the main jape rules");
    			System.exit(1);
            }
        }
        
        parameters.put("japeMainPath", japeMainPath);
        
        
        if(!execution) {
        	System.out.println("No gazzeter or Jape Rules were provided. There is nothing to do. Please review your configuration");
        	System.exit(1);
        }
        
        File outputDirectory = new File(outputFilePath);
	    if(!outputDirectory.exists())
	    	outputDirectory.mkdirs();
	    parameters.put("outputDirectory", outputFilePath);
	    
	    String baseGateApplicationPath = "gate_application/application_exported/application.xgapp";
	    parameters.put("gateApplicationPath", workdirPath + baseGateApplicationPath);
	    
	    
	    Set<String> processedFiles = null;
	    try {
	    	processedFiles = getFiles(outputFilePath);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    Integer threads = 1;
	    if(threads_str!=null) {
	    	try {
	    	  threads = Integer.parseInt(threads_str.trim());
	        } catch (NumberFormatException nfe) {
	          System.out.println("NumberFormatException: " + nfe.getMessage());
	        }
	    }
	    
	    try {
			Gate.init();
		} catch (GateException e) {
			System.out.println("App :: main :: Gate Exception  ");
			e.printStackTrace();
			System.exit(1);
		} 
 
	    try {
	    	process(threads, parameters, processedFiles);
		} catch (GateException e) {
			System.out.println("App :: main :: GateException :: " +  e.getMessage());
			System.out.println(e);
			e.printStackTrace(System.out);
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			System.out.println("App :: main :: IOException :: " +  e.getMessage());
			System.out.println(e);
			e.printStackTrace(System.out);
			e.printStackTrace();
			System.exit(1);
		}

	}

    /**
     * Annotation Process
     * @param inputDirectory
     * @param outputDirectory
     * @throws GateException
     * @throws IOException
     */
    private static void process(Integer threads, Map<String,String> parameters,Set<String> processedFiles) throws GateException, IOException {
    	try {
    		System.out.println("App :: process :: INIT PROCESS");
	    	File directory = new File(parameters.get("inputFilePath")); 
	    	
	    	//This is the old way to register plugins, this needs internet access, not always allowed for batch applications.
	    	//Plugin anniePlugin = new Plugin.Maven("uk.ac.gate.plugins", "annie", "8.6"); 
	    	//Gate.getCreoleRegister().registerPluTgin(anniePlugin); 

	    	//load a gate auxiliar application save to the Cloud in order to read the maven local plugins for batch application. This works without internet access.
	    	//https://groups.io/g/gate-users/topic/67034883?p=Created,,,20,2,0,0
	    	//A work around complex because of the GATE architectural issues.
	    	//String application = "/home/javi/GATE_Developer_8.6.1/application_exported/application_exported/application.xgapp";
	    	String application = parameters.get("gateApplicationPath"); 
		    try {	
		    	PersistenceManager.loadObjectFromFile(new File(application));
	    	}catch(Exception e) {
	    		System.out.println("App :: process :: ERROR loadin gate application form cloud. Please review the path of the application");
				System.out.println(e);
				e.printStackTrace(System.out);
				e.printStackTrace();
				System.exit(1);
	    		
		    }	 

		    Gate.getCreoleRegister().registerPlugin(new Plugin.Maven("uk.ac.gate.plugins", "format-bdoc", "1.10"));
    		
	    	File[] files =  directory.listFiles();
			int len = files.length;
			if(len<threads) {
				System.out.println("App :: process :: total files less than threads numbers, only one thread will be launch ");
				threads = 1;
			}
			//total work for thread
			int x = len/threads; 
			//plus a remaining to be assign to the last one.
			int m = len % threads; 
			
//			List<File[]> list2 = new ArrayList<File[]>();
//			for (int i = 0; i < threads - x + 1; i += x)
//				list2.add(Arrays.copyOfRange(files, i, i + x));
			
			List<File[]> list2 = new ArrayList<File[]>();
			for (int i = 0, j = 0; i < threads; i +=1, j += x)
				list2.add(Arrays.copyOfRange(files, j, j + x));


			//add to the last thread the remaining
			if(m!=0) {
				File[] tail = Arrays.copyOfRange(files, len - m, len);
				File[] already_assign_to_last_thread = list2.get(threads-1);
				File[] the_two = (File[])ArrayUtils.addAll(already_assign_to_last_thread, tail);
				list2.remove(threads-1);
				list2.add(the_two);
			}
				
			long begTest = new java.util.Date().getTime();
			List<Future> futuresList = new ArrayList<Future>();
			ExecutorService eservice = Executors.newFixedThreadPool(10);
			for(int index = 0; index < threads; index++) {
				System.out.println("App :: main :: CREATE PROCESS = " + index);
				futuresList.add(eservice.submit(new Process(index, list2.get(index), processedFiles, parameters)));
				System.out.println("App :: main :: CREATED PROCESS = " + index);
			}
			
			System.out.println("App :: main :: WAIT FOR PROCESS RESULTS ");
			
			Object taskResult;
			for(Future future:futuresList) {
				try {
					taskResult = future.get();
					System.out.println("App :: main :: TASK RESULT =  " + taskResult);
				}catch (InterruptedException e) {
					System.out.println("App :: main :: InterruptedException :: " + e.getMessage());
		    		System.out.println(e);
					e.printStackTrace(System.out);
					e.printStackTrace();
					System.exit(1);
				}catch (ExecutionException e) {
					System.out.println("App :: main :: ExecutionException :: " + e.getMessage());
		    		System.out.println(e);
					e.printStackTrace(System.out);
					e.printStackTrace();
					System.exit(1);
				}
			}
			Double secs = new Double((new java.util.Date().getTime() - begTest)*0.001);
			System.out.println("App :: main :: EXECUTION TIME:  " + secs + " seconds");
	    	 
			//Gate.removeKnownPlugin(anniePlugin);

    	}catch(Exception e) {
    		System.out.println("App :: main :: Exception :: " + e.getMessage());
    		System.out.println(e);
			e.printStackTrace(System.out);
			e.printStackTrace();
			System.exit(1);
	    }	 
	    System.out.println("App :: main :: END PROCESS");
	    System.exit(0);
    }
    
    /**
     * Return a set of files 
     * @param dir
     * @return
     * @throws IOException
     */
	public static Set<String> getFiles(String dir) throws IOException {
	    Set<String> fileList = new HashSet<>();
	    try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
	        for (Path path : stream) {
	            if (!Files.isDirectory(path)) {
	                fileList.add(FileUtils.removeExtension(path.getFileName().toString()));
	            }
	        }
	    }
	    return fileList;
	}
    
    /**
     * Basic unzipping folder method
     * @param input
     * @param output
     * @throws IOException
     */
    private static void unZipIt(String zipFile, String outputFolder){
    	System.out.println("App :: unZipIt :: INIT");
    	byte[] buffer = new byte[1024];
        try{
	       	//create output directory is not exists
	       	File folder = new File(outputFolder);
	       	if(!folder.exists()){
	       		folder.mkdir();
	       	}
	       	//get the zip file content
	       	ZipInputStream zis =
	       		new ZipInputStream(new FileInputStream(zipFile));
	       	//get the zipped file list entry
	       	ZipEntry ze = zis.getNextEntry();
	       	if(ze==null) {
	       		System.out.println("Error unziping file, please review if you zip file provided is not corrupt file remember that it must contain a list.def file inside.");
               	System.exit(1);
	       	}
	       	while(ze!=null){
	       	   String fileName = ze.getName();
	           File newFile = new File(outputFolder + File.separator + fileName);
	           System.out.println("file unzip : "+ newFile.getAbsoluteFile());
	           //create all non exists folders
	           //else you will hit FileNotFoundException for compressed folder
	           new File(newFile.getParent()).mkdirs();
	           FileOutputStream fos = new FileOutputStream(newFile);
	           int len;
	           while ((len = zis.read(buffer)) > 0) {
	        	   fos.write(buffer, 0, len);
	           }
	           fos.close();
	           ze = zis.getNextEntry();
	       	}
	       	zis.closeEntry();
	       	zis.close();
	       	System.out.println("App :: unZipIt :: END");
       }catch(IOException e){
    	   System.out.println("App :: unZipIt :: IOException :: " + e.getMessage());
   		   System.out.println(e);
			e.printStackTrace(System.out);
			e.printStackTrace();
			System.exit(1);
       }
    }
    
}
