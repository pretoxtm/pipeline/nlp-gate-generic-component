package es.bsc.inb.nlp.gate.generic.component.util;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.creole.ResourceInstantiationException;
import gate.util.InvalidOffsetException;

/**
 * 
 * @author javi
 *
 */
public  class AnnotationUtil {
	
	static final String template_value_name = "value";
	
	static final String send_code_name = "send_code";
	
	static final String send_codelist_name = "send_codelist";
	
	static final String send_domain_code_name = "send_domain_code";
	
	public static Integer right_limit = 50;
	
	public static Integer left_limit = 50;
	
	private static Integer finding_id = 1;
	
	private static String aux_docName = "";
	
	
	/**
	 * 
	 * @param doc
	 * @param fields
	 * @param finding
	 * @param type
	 * @param left_limit
	 * @param right_limit
	 * @param avoid_token_between
	 * @return
	 */
	public static Annotation getClosestAnnotation(AnnotationSet fields, Annotation finding, String type, Integer left_limit, Integer right_limit, String avoid_token_between) {
		//AnnotationSet treatment_related_triggers = fields.get(type, sentence.getStartNode().getOffset(), sentence.getEndNode().getOffset());
		AnnotationSet annotations_type = fields.get(type);
		Annotation closest = null;
		Integer token_between_closest = 10000;
		Boolean beforeFinding = false;
		for (Annotation annotation_type : annotations_type) {
			//System.out.println(type + ": " + gate.Utils.stringFor(doc, annotation_type));
			//si el finding esta despues del trigger
			AnnotationSet token_between = null;
			if (finding.getStartNode().getOffset() > annotation_type.getEndNode().getOffset()) {
				//before the finding
				token_between =  fields.get("Token", annotation_type.getEndNode().getOffset(), finding.getStartNode().getOffset());
				if(token_between.size()<=left_limit) {
					beforeFinding = true;
				}else {
					token_between = null;
				}
			}else if (annotation_type.getStartNode().getOffset() > finding.getEndNode().getOffset()) {
				//after the finding 
				token_between =  fields.get("Token", finding.getEndNode().getOffset(), annotation_type.getStartNode().getOffset());
				if(token_between.size()>right_limit) {
					token_between = null;
				}
			}else {
				closest = annotation_type;
				token_between_closest = 0;
			}
			//if token do not contain in the middle specific tokens and the quantity of tokens is less than the closest one or are the same quantity but the closest one is not from the left side ....
			if(token_between!=null && tokensDoNotContain(token_between, avoid_token_between) && ((token_between.size()<token_between_closest) || token_between.size()==token_between_closest && !beforeFinding)) {
				closest = annotation_type;
				token_between_closest = token_between.size();
			}
		}
		return closest;
	}
	
	/**
	 * Return the closes entity with an intermediate POS. This rule apply to the right of the mayor entity.  
	 * @param doc
	 * @param fields
	 * @param finding
	 * @param type
	 * @param left_limit
	 * @param right_limit
	 * @param avoid_token_between
	 * @return
	 */
	public static Annotation getClosestAnnotationWithPOSPriorityRight(AnnotationSet fields, Annotation finding, String type, Integer right_limit, String avoid_token_between, String POS) {
		AnnotationSet annotations_type = fields.get(type);
		Annotation closest = null;
		Integer token_between_closest = 10000;
		for (Annotation annotation_type : annotations_type) {
			AnnotationSet token_between = null;
			if (annotation_type.getStartNode().getOffset() > finding.getEndNode().getOffset()) {
				//after the finding 
				token_between =  fields.get("Token", finding.getEndNode().getOffset(), annotation_type.getStartNode().getOffset());
				if(token_between.size()<=right_limit && tokensIncludePOS(token_between, POS)) {
					//review closest of rule
					if(token_between!=null && token_between.size()<token_between_closest) {
						closest = annotation_type;
						token_between_closest = token_between.size();
					}
				}
			}
		}
		return closest;
	}
	
	
	/**
	 * Return try if the list of token has a POS 
	 * @param token_between
	 * @param pOS
	 * @return
	 */
	private static boolean tokensIncludePOS(AnnotationSet token_between, String POS) {
		if(POS==null || POS.contentEquals("")) {
			return false;
		}else { 
			for (Annotation annotation : token_between) {
				if(annotation.getFeatures().get("pos").toString().startsWith(POS)) {
					return true;
				}
			}
		}	
		return false;
	}

	/**
	 * 
	 * @param token_between
	 * @param avoid_token_between
	 * @return
	 */
	private static boolean tokensDoNotContain(AnnotationSet token_between, String avoid_token_between) {
		if(avoid_token_between==null || avoid_token_between.contentEquals("")) {
			return true;
		}else { 
			for (Annotation annotation : token_between) {
				if(annotation.getFeatures().get("word").equals(avoid_token_between)) {
					return false;
				}
			
			}
		}	
		return true;
	}
	
	
	public static Annotation getManifestationOfFindingAnnotation(AnnotationSet fields, Annotation finding, String avoid_token_between) {
		Integer left_limit = 10;
		Integer right_limit= 5;
		AnnotationSet annotations_type = fields.get("MANIFESTATION_FINDING");
		List<String> only_send_codes = new ArrayList<>(Arrays.asList("I", "D","C"));
		Annotation closest = null;
		Integer token_between_closest = 10000;
		Boolean beforeFinding = false;
		for (Annotation annotation_type : annotations_type) {
			String send_code = getOnlySENDCode(annotation_type, "");
			if(only_send_codes.contains(send_code)) {
				//System.out.println(type + ": " + gate.Utils.stringFor(doc, annotation_type));
				//si el finding esta despues del trigger
				AnnotationSet token_between = null;
				if (finding.getStartNode().getOffset() > annotation_type.getEndNode().getOffset()) {
					//before the finding
					token_between =  fields.get("Token", annotation_type.getEndNode().getOffset(), finding.getStartNode().getOffset());
					long characters_between = finding.getStartNode().getOffset() - annotation_type.getEndNode().getOffset();
					//just in case of small tokens like , ( and so on
					if(token_between.size()<=left_limit) {
						beforeFinding = true;
					}else if(characters_between < 50){
						beforeFinding = true;
					}else {
						token_between = null;
					}
				}else if (annotation_type.getStartNode().getOffset() > finding.getEndNode().getOffset()) {
					//after the finding 
					token_between =  fields.get("Token", finding.getEndNode().getOffset(), annotation_type.getStartNode().getOffset());
					if(token_between.size()>right_limit) {
						token_between = null;
					}
				}else {
					closest = annotation_type;
					token_between_closest = 0;
				}
				//if token do not contain in the middle specific tokens and the quantity of tokens is less than the closest one or are the same quantity but the closest one is not from the left side ....
				if(token_between!=null && tokensDoNotContain(token_between, avoid_token_between) && ((token_between.size()<token_between_closest) || token_between.size()==token_between_closest && !beforeFinding)) {
					closest = annotation_type;
					token_between_closest = token_between.size();
				}
			}else {
				//System.out.println("testear");
			}
		}
		return closest;
	}
	
	private static String getOnlySENDCode(Annotation annotation, String text) {
		String send_code = "";
		if(annotation.getFeatures().get("MANUAL_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("MANUAL_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_SEND_DOMAIN_CODE").toString();
		}else if(annotation.getFeatures().get("CDISC_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("CDISC_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("ETOX_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("ETOX_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("ETOX_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("ETOX_SEND_DOMAIN_CODE").toString();
		}
		return send_code;
	}
	
	
	/**
	 * Execute process in a document
	 * @param inputFile
	 * @param outputGATEFile
	 * @throws ResourceInstantiationException
	 * @throws IOException 
	 * @throws JsonGenerationException 
	 * @throws InvalidOffsetException
	 */
	public static void processFindingRelation(AnnotationSet sentenceFields, Annotation sentence, Annotation finding,  AnnotationSet as, String annotationSetRelationExtraction) {
		try {
			
			Document doc = as.getDocument();
			if(!doc.getName().equals(aux_docName)) {
				finding_id = 1;
				aux_docName = doc.getName();
				System.out.println("processFindingRelation :: document : " + aux_docName);
			}
			
			finding.getFeatures().put("ANNOTATION_TYPE",finding.getType());
			finding.getFeatures().put(template_value_name, getSendCode(finding, gate.Utils.stringFor(doc, finding)));
			finding.getFeatures().put(send_code_name, getOnlySendCodeFinding(finding, gate.Utils.stringFor(doc, finding)));
			finding.getFeatures().put(send_codelist_name, getOnlySENDCodeList(finding, gate.Utils.stringFor(doc, finding)));
			finding.getFeatures().put(send_domain_code_name, getOnlySendDomainCodeFinding(finding, gate.Utils.stringFor(doc, finding)));
			doc.getAnnotations(annotationSetRelationExtraction).add(finding.getStartNode(), finding.getEndNode(), "FINDING_"+finding_id, finding.getFeatures());
			
			sentence.getFeatures().put("ANNOTATION_TYPE", "RELEVANT_TEXT");
			//doc.getAnnotations(annotationSetRelationExtraction).add(sentence.getStartNode(), sentence.getEndNode(), "FINDING_"+finding_id, sentence.getFeatures());
					
			if(finding.getFeatures().get("IS_TREATMENT_RELATED")!=null) {
				Annotation is_treatment_related = as.get(new Integer(finding.getFeatures().get("IS_TREATMENT_RELATED").toString()));
				is_treatment_related.getFeatures().put("ANNOTATION_TYPE", "IS_TREATMENT_RELATED");
				is_treatment_related.getFeatures().put(template_value_name, is_treatment_related.getFeatures().get("SEND_CODE"));
				doc.getAnnotations(annotationSetRelationExtraction).add(is_treatment_related.getStartNode(), is_treatment_related.getEndNode(), "FINDING_"+finding_id, is_treatment_related.getFeatures());
			}else {
				FeatureMap features_t = Factory.newFeatureMap();
				features_t.put(template_value_name, "Y");
				features_t.put("ANNOTATION_TYPE", "IS_TREATMENT_RELATED");
				doc.getAnnotations(annotationSetRelationExtraction).add(finding.getStartNode(), finding.getEndNode(), "FINDING_"+finding_id, features_t);
			}
			
			if (finding.getFeatures().get("MANIFESTATION_FINDING")!=null && !finding.getFeatures().get("MANIFESTATION_FINDING").toString().equals("")) {
				Annotation manifestation = as.get(new Integer(finding.getFeatures().get("MANIFESTATION_FINDING").toString()));
				manifestation.getFeatures().put(template_value_name, getSendCode(manifestation, gate.Utils.stringFor(doc, manifestation)));
				manifestation.getFeatures().put(send_code_name, getOnlySENDCode(manifestation, gate.Utils.stringFor(doc, manifestation)));
				manifestation.getFeatures().put("ANNOTATION_TYPE",manifestation.getType());
				doc.getAnnotations(annotationSetRelationExtraction).add(manifestation.getStartNode(), manifestation.getEndNode(), "FINDING_"+finding_id, manifestation.getFeatures());
			}
			
//			if(finding.getType().equals("FINDING")) {
//				if (finding.getFeatures().get("MANIFESTATION_FINDING")!=null && !finding.getFeatures().get("MANIFESTATION_FINDING").toString().equals("")) {
//					Annotation manifestation = as.get(new Integer(finding.getFeatures().get("MANIFESTATION_FINDING").toString()));
//					manifestation.getFeatures().put("ANNOTATION_TYPE",manifestation.getType());
//					doc.getAnnotations(annotationSetRelationExtraction).add(manifestation.getStartNode(), manifestation.getEndNode(), "FINDING_"+finding_id, manifestation.getFeatures());
//				}
//			}else if (finding.getType().equals("STUDY_TESTCD")){
//				if (finding.getFeatures().get("MANIFESTATION_FINDING")!=null && !finding.getFeatures().get("MANIFESTATION_FINDING").toString().equals("")) {
//					Annotation manifestation = as.get(new Integer(finding.getFeatures().get("MANIFESTATION_FINDING").toString()));
//					manifestation.getFeatures().put("ANNOTATION_TYPE",manifestation.getType());
//					doc.getAnnotations(annotationSetRelationExtraction).add(manifestation.getStartNode(), manifestation.getEndNode(), "FINDING_"+finding_id, manifestation.getFeatures());
//				}
//			}

			
			if (finding.getFeatures().get("GROUP")!=null && !finding.getFeatures().get("GROUP").toString().equals("")) {
				Annotation group = as.get(new Integer(finding.getFeatures().get("GROUP").toString()));
				group.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, group));
				group.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, group));
				group.getFeatures().put("ANNOTATION_TYPE",group.getType());
				doc.getAnnotations(annotationSetRelationExtraction).add(group.getStartNode(), group.getEndNode(), "FINDING_"+finding_id, group.getFeatures());
			}
			
			List<Integer> findings_id_copy = new ArrayList<>();
			findings_id_copy.add(finding_id);
			
			
			if (finding.getFeatures().get("SPECIMEN")!=null && !finding.getFeatures().get("SPECIMEN").toString().equals("")) {
				//Specimen relation process
				String specimens = finding.getFeatures().get("SPECIMEN").toString();
				String[] specimens_splited = specimens.split(",");
				Boolean first = true;
				for (String spe_id : specimens_splited) {
					if(!first) {
						//if is not the first. Only enter when there is more than one specimen. 
						//Need to make copy of FINDING and increment finding id
						//get all atributes from actual finding and replicate annotations
						Integer finding_ant_id = finding_id;
						finding_id = finding_id + 1;
						AnnotationSet finding_atributes = doc.getAnnotations(annotationSetRelationExtraction).get("FINDING_"+finding_ant_id);
						for (Annotation annotation : finding_atributes) {
							if(!annotation.getFeatures().get("ANNOTATION_TYPE").toString().equals("SPECIMEN")) {
								doc.getAnnotations(annotationSetRelationExtraction).add(annotation.getStartNode(), annotation.getEndNode(), "FINDING_"+finding_id, annotation.getFeatures());
							}
						}
						findings_id_copy.add(finding_id);
					}else {
						first = false;
					}
					try {
						//add specimen to finding
						Annotation specimen = as.get(new Integer(spe_id));
						specimen.getFeatures().put(template_value_name, getSendCode(specimen, gate.Utils.stringFor(doc, specimen)));
						specimen.getFeatures().put(send_code_name, getOnlySENDCode(specimen, gate.Utils.stringFor(doc, specimen)));
						specimen.getFeatures().put(send_codelist_name, getOnlySENDCodeList(specimen, gate.Utils.stringFor(doc, specimen)));
						specimen.getFeatures().put("ANNOTATION_TYPE",specimen.getType());
						doc.getAnnotations(annotationSetRelationExtraction).add(specimen.getStartNode(), specimen.getEndNode(), "FINDING_"+finding_id, specimen.getFeatures());
					}catch(Exception e) {
						e.printStackTrace();
					}
					
				}
			}
					
				
				
				
			
			
//			Annotation STUDY_DOMAIN = getClosestAnnotation(doc,  sentenceFields, finding, "STUDY_DOMAIN", right_limit, left_limit, null);
//			if(STUDY_DOMAIN!=null) {
//				System.out.println("STUDY_DOMAIN: " + gate.Utils.stringFor(doc, STUDY_DOMAIN));
//				STUDY_DOMAIN.getFeatures().put("ANNOTATION_TYPE",STUDY_DOMAIN.getType());
//				STUDY_DOMAIN.getFeatures().put(template_value_name, getSendCode(STUDY_DOMAIN, gate.Utils.stringFor(doc, STUDY_DOMAIN)));
//				STUDY_DOMAIN.getFeatures().put(send_code_name, getOnlySENDCode(STUDY_DOMAIN, gate.Utils.stringFor(doc, STUDY_DOMAIN)));
//				STUDY_DOMAIN.getFeatures().put(send_codelist_name, getOnlySENDCodeList(STUDY_DOMAIN, gate.Utils.stringFor(doc, STUDY_DOMAIN)));
//				doc.getAnnotations(annotationSetRelationExtraction).add(STUDY_DOMAIN.getStartNode(), STUDY_DOMAIN.getEndNode(), "FINDING_"+finding_id, STUDY_DOMAIN.getFeatures());
//			}
			
			//NOAL do not came with a specific finding is a general information
			
//			Annotation risk_level = getClosestAnnotation(doc,  sentenceFields, finding, "RISK_LEVEL", right_limit, left_limit, null);
//			if(risk_level!=null) {
//				System.out.println("RISK_LEVEL: " + gate.Utils.stringFor(doc, risk_level));
//				//risk_level.getFeatures().put(template_value_name, getSendCode(risk_level, gate.Utils.stringFor(doc, risk_level)));
//				risk_level.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, risk_level));
//				risk_level.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, risk_level));
//				risk_level.getFeatures().put("ANNOTATION_TYPE",risk_level.getType());
//				doc.getAnnotations(annotationSetRelationExtraction).add(risk_level.getStartNode(), risk_level.getEndNode(), "FINDING_"+finding_id, risk_level.getFeatures());
//			}
			
			

//			Annotation STUDY_DAY_FINDING = getClosestAnnotation(doc,  sentenceFields, finding, "STUDY_DAY_FINDING", right_limit, left_limit, null);
//			if(STUDY_DAY_FINDING!=null) {
//				System.out.println("STUDY_DAY_FINDING: " + gate.Utils.stringFor(doc, STUDY_DAY_FINDING));
//				STUDY_DAY_FINDING.getFeatures().put("ANNOTATION_TYPE",STUDY_DAY_FINDING.getType());
//				STUDY_DAY_FINDING.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, STUDY_DAY_FINDING));
//				STUDY_DAY_FINDING.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, STUDY_DAY_FINDING));
//				doc.getAnnotations(annotationSetRelationExtraction).add(STUDY_DAY_FINDING.getStartNode(), STUDY_DAY_FINDING.getEndNode(), "FINDING_"+finding_id, STUDY_DAY_FINDING.getFeatures());
//			}
			
			//if there is already a finding related to a dose_sex then
			for (Integer finding_id_to_process : findings_id_copy) {
				//iterate over each finding in collection
				if(finding.getFeatures().get("DOSE_SEX")!=null || finding.getFeatures().get("DOSE_SEX_MULTI")!=null ) {
					if(finding.getFeatures().get("DOSE_SEX_MULTI")!=null && !finding.getFeatures().get("DOSE_SEX_MULTI").toString().equals("")) {
						//multi analysis
						Annotation dose_sex_multi = as.get(new Integer(finding.getFeatures().get("DOSE_SEX_MULTI").toString()));
						AnnotationSet dose_sex_set = as.get("DOSE_SEX", dose_sex_multi.getStartNode().getOffset(), dose_sex_multi.getEndNode().getOffset());
						//por cada dosis y sexo relaciono
						Boolean first =true;
						for (Annotation dose_sex : dose_sex_set) {
							Annotation dose = as.get("DOSE", dose_sex.getStartNode().getOffset(), dose_sex.getEndNode().getOffset()).iterator().next();
							Annotation sex = as.get("SEX", dose_sex.getStartNode().getOffset(), dose_sex.getEndNode().getOffset()).iterator().next();
							if(!first) {
								//Need to make copy of FINDING and increment finding id
								int finding_ant = finding_id_to_process;
								finding_id = finding_id + 1;
								//get all atributes from actual finding and replicate annotations
								AnnotationSet finding_atributes = doc.getAnnotations(annotationSetRelationExtraction).get("FINDING_"+finding_ant);
								for (Annotation annotation : finding_atributes) {
									if(!annotation.getFeatures().get("ANNOTATION_TYPE").toString().equals("SEX") && !annotation.getFeatures().get("ANNOTATION_TYPE").toString().equals("DOSE")) {
										doc.getAnnotations(annotationSetRelationExtraction).add(annotation.getStartNode(), annotation.getEndNode(), "FINDING_"+finding_id, annotation.getFeatures());
									}
								}
								finding_id_to_process = finding_id;
							}else {
								first = false;
							}
							dose.getFeatures().put("ANNOTATION_TYPE",dose.getType());
							dose.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, dose));
							dose.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, dose));
							doc.getAnnotations(annotationSetRelationExtraction).add(dose.getStartNode(), dose.getEndNode(), "FINDING_"+finding_id_to_process, dose.getFeatures());
							String send_code = getSendCode(sex, gate.Utils.stringFor(doc, sex));
							sex.getFeatures().put(template_value_name, send_code);
							sex.getFeatures().put(send_code_name, getOnlySENDCode(sex, gate.Utils.stringFor(doc, sex)));
							sex.getFeatures().put("ANNOTATION_TYPE",sex.getType());
							doc.getAnnotations(annotationSetRelationExtraction).add(sex.getStartNode(), sex.getEndNode(), "FINDING_"+finding_id_to_process, sex.getFeatures());
						}
					}else if (finding.getFeatures().get("DOSE_SEX")!=null && !finding.getFeatures().get("DOSE_SEX").toString().equals("")) {
						// dose sex analysis
						Annotation dose_sex = as.get(new Integer(finding.getFeatures().get("DOSE_SEX").toString()));
						Annotation dose = as.get("DOSE", dose_sex.getStartNode().getOffset(), dose_sex.getEndNode().getOffset()).iterator().next();
						Annotation sex = as.get("SEX", dose_sex.getStartNode().getOffset(), dose_sex.getEndNode().getOffset()).iterator().next();
						
						dose.getFeatures().put("ANNOTATION_TYPE",dose.getType());
						dose.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, dose));
						dose.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, dose));
						doc.getAnnotations(annotationSetRelationExtraction).add(dose.getStartNode(), dose.getEndNode(), "FINDING_"+finding_id_to_process, dose.getFeatures());
						
						String send_code = getSendCode(sex, gate.Utils.stringFor(doc, sex));
						sex.getFeatures().put(template_value_name, send_code);
						sex.getFeatures().put(send_code_name, getOnlySENDCode(sex, gate.Utils.stringFor(doc, sex)));
						sex.getFeatures().put("ANNOTATION_TYPE",sex.getType());
						doc.getAnnotations(annotationSetRelationExtraction).add(sex.getStartNode(), sex.getEndNode(), "FINDING_"+finding_id_to_process, sex.getFeatures());
					}
					
					
				}else { 
					if (finding.getFeatures().get("DOSE")!=null && !finding.getFeatures().get("DOSE").toString().equals("")) {
						Annotation dose = as.get(new Integer(finding.getFeatures().get("DOSE").toString()));
						dose.getFeatures().put("ANNOTATION_TYPE",dose.getType());
						dose.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, dose));
						dose.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, dose));
						doc.getAnnotations(annotationSetRelationExtraction).add(dose.getStartNode(), dose.getEndNode(), "FINDING_"+finding_id_to_process, dose.getFeatures());
					}else if (finding.getFeatures().get("DOSE_QUALIFICATION")!=null && !finding.getFeatures().get("DOSE_QUALIFICATION").toString().equals("")) {
						Annotation dose_qualification = as.get(new Integer(finding.getFeatures().get("DOSE_QUALIFICATION").toString()));
						dose_qualification.getFeatures().put("ANNOTATION_TYPE","DOSE");
						dose_qualification.getFeatures().put(template_value_name, gate.Utils.stringFor(doc, dose_qualification));
						dose_qualification.getFeatures().put(send_code_name, gate.Utils.stringFor(doc, dose_qualification));
						doc.getAnnotations(annotationSetRelationExtraction).add(dose_qualification.getStartNode(), dose_qualification.getEndNode(), "FINDING_"+finding_id_to_process, dose_qualification.getFeatures());
					}
					if (finding.getFeatures().get("SEX")!=null && !finding.getFeatures().get("SEX").toString().equals("")) {
						Annotation sex = as.get(new Integer(finding.getFeatures().get("SEX").toString()));
						String send_code = getSendCode(sex, gate.Utils.stringFor(doc, sex));
						sex.getFeatures().put(template_value_name, send_code);
						sex.getFeatures().put(send_code_name, getOnlySENDCode(sex, gate.Utils.stringFor(doc, sex)));
						sex.getFeatures().put("ANNOTATION_TYPE",sex.getType());
						doc.getAnnotations(annotationSetRelationExtraction).add(sex.getStartNode(), sex.getEndNode(), "FINDING_"+finding_id_to_process, sex.getFeatures());
					}
				}
			}

	  		
	  	finding_id = finding_id + 1;
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
    }
	
	
	private static String getSendCode(Annotation annotation, String text) {
		String send_code = "";
		if(annotation.getFeatures().get("MANUAL_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("MANUAL_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_SEND_DOMAIN_CODE").toString();
		}else if(annotation.getFeatures().get("CDISC_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("CDISC_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("ETOX_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("ETOX_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("ETOX_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("ETOX_SEND_DOMAIN_CODE").toString();
		}
		if(!send_code.equals("")) {
			return text+"("+send_code+")";
		}
		return text;
	}
	
	private static String getOnlySENDCodeList(Annotation annotation, String text) {
		String send_code = "";
		if(annotation.getFeatures().get("MANUAL_CODELIST")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_CODELIST").toString();
		}else if(annotation.getFeatures().get("CDISC_CODELIST")!=null) {
			send_code = annotation.getFeatures().get("CDISC_CODELIST").toString();
		}else if(annotation.getFeatures().get("ETOX_CODELIST")!=null) {
			send_code = annotation.getFeatures().get("ETOX_CODELIST").toString();
		}else if(annotation.getFeatures().get("SMALLER_TERM_CODELIST")!=null) {
			send_code = annotation.getFeatures().get("SMALLER_TERM_CODELIST").toString();
		}
		return send_code;
	}
	
	
	
	/**
	 * Return only the SEND CODE from the DOMAIN of the finding.
	 * Example: CL, LB, MA, MI, OM, PP and TF
	 * @param annotation
	 * @param text
	 * @return
	 */
	private static String getOnlySendDomainCodeFinding(Annotation annotation, String text) {
		String send_code = "";
		if(annotation.getFeatures().get("CDISC_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("CDISC_SEND_DOMAIN_CODE").toString();
		}else if(annotation.getFeatures().get("ETOX_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("ETOX_SEND_DOMAIN_CODE").toString();
		}else if(annotation.getFeatures().get("MANUAL_SEND_DOMAIN_CODE")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_SEND_DOMAIN_CODE").toString();
		}else if(annotation.getFeatures().get("SMALLER_TERM_SEND_DOMAIN_CODE")!=null){
			send_code = annotation.getFeatures().get("SMALLER_TERM_SEND_DOMAIN_CODE").toString();
		}
		return send_code;
	}
	
	/**
	 * Return only the SEND CODE of the FINDING OR STUDY_TESTCD
	 * Example: GLUC,INSULIN
	 * @param annotation
	 * @param text
	 * @return
	 */
	private static String getOnlySendCodeFinding(Annotation annotation, String text) {
		String send_code = "";
		if(annotation.getFeatures().get("MANUAL_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("MANUAL_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("CDISC_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("CDISC_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("ETOX_SEND_CODE")!=null) {
			send_code = annotation.getFeatures().get("ETOX_SEND_CODE").toString();
		}else if(annotation.getFeatures().get("SMALLER_TERM_SEND_CODE")!=null){
			send_code = annotation.getFeatures().get("SMALLER_TERM_SEND_CODE").toString();
		}
		return send_code;

	}
}



   




